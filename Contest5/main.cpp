#include <iostream>
#include <math.h>

template <typename T>
T * access2dArray( T * array, int x, int y, int width )
{
    return &array[ x + y * width ];
}

template <typename T>
void copyArraySection(T * arr, int x1, int y1, int w, int h, int x2, int y2, int arrayWidth)
{
    for (int i = 0; i < w; ++i) {
        for (int j = 0; j < h; ++j) {
            *access2dArray( arr, i + x2, j + y2, arrayWidth ) = *access2dArray( arr, i + x1, j + y1, arrayWidth );
        }
    }
}

void fractal( bool * field, int fieldSize, int currDepth, int targetDepth )
{
    std::cout << currDepth << std::endl;
    if( currDepth > targetDepth )
        return;

    int crossSize = pow(3, currDepth - 1);

    int x = floor(fieldSize / 2);
    int y = floor(fieldSize / 2);

    int x0 = x - floor(crossSize / 2.);
    int y0 = y - floor(crossSize / 2.);

    int x1 = x0 - crossSize;
    int y1 = y0;

    int x2 = x0;
    int y2 = y0 - crossSize;

    int x3 = x0 + crossSize;
    int y3 = y0;

    int x4 = x0;
    int y4 = y0 + crossSize;

    copyArraySection( field, x0, y0, crossSize, crossSize, x1, y1, fieldSize );
    copyArraySection( field, x0, y0, crossSize, crossSize, x2, y2, fieldSize );
    copyArraySection( field, x0, y0, crossSize, crossSize, x3, y3, fieldSize );
    copyArraySection( field, x0, y0, crossSize, crossSize, x4, y4, fieldSize );
    

    fractal( field, fieldSize, currDepth + 1, targetDepth );
}

int main()
{
    int depth;
    std::cin >> depth;

    int size = pow(3, depth);

    bool * field = (bool *)malloc( sizeof(bool) * size * size );

    memset( field, 0, size*size );

    field[ size * size / 2 ] = 1;

    fractal( field, size, 1, depth );

    for (int x = 0; x < size; ++x)
    {
        for (int y = 0; y < size; ++y)
        {
                std::cout << ( field[ x + y * size ] ? "*" : " " );
        }
        std::cout << std::endl;
    }
}
