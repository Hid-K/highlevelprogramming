#include <iostream>
#include <fstream>
#include <stdint.h>
#include <vector>
#include <string>
#include <algorithm>
#include <cassert>
#include <cstring>
#include <regex>
#include <list>
#include <numeric>
#include <ranges>

class Game;
class Team;

class Team
{
    private:
        std::string _name;
        uint _wins;
        uint _losses;
        uint _ties;
        std::vector<Game*> _games;

    public:
        Team(std::string name, uint wins, uint losses, uint ties) noexcept :
            _name( name ),
            _wins( wins ),
            _losses( losses ),
            _ties( ties )
            {}

        std::string getName() const noexcept
        {
            return _name;
        }

        uint getWins() const noexcept
        {
            return _wins;
        }

        uint getLosses() const noexcept
        {
            return _losses;
        }

        uint getTies() const noexcept
        {
            return _ties;
        }

        const std::vector<Game*>& getGames() const noexcept
        {
            return _games;
        }

        void addGame( Game* game )
        {
            _games.push_back( game );
        }
};

class Game
{
    private:
        Team& _team1;
        Team& _team2;
        uint _team1Score;
        uint _team2Score;

    public:
        Game(Team& team1, Team& team2, uint team1Score, uint team2Score) noexcept :
            _team1( team1 ),
            _team2( team2 ),
            _team1Score( team1Score ),
            _team2Score( team2Score )
            {
                _team1.addGame( this );
                _team2.addGame( this );
            }

        Team& getTeam1() const noexcept
        {
            return _team1;
        }

        Team& getTeam2() const noexcept
        {
            return _team2;
        }

        uint getTeam1Score() const noexcept
        {
            return _team1Score;
        }

        uint getTeam2Score() const noexcept
        {
            return _team2Score;
        }
};

int main()
{
    const std::regex gameRegex( "([A-Za-z]+) - ([A-Za-z]+) (\\d+):(\\d+)" );
    std::ifstream in("input.txt");

    if( !in.is_open() )
    {
        std::cerr << "Error opening file: input.txt does not exists or you don't have a reading rights on it!" << std::endl;
        return 1;
    }

    std::list<Team> teams;
    std::list<Game> games;
    
    for( std::string line; getline( in, line ); )
    {
        std::cout << "Parsing line: " << line << std::endl;

        std::string team1;
        std::string team2;
        int score1;
        int score2;
        
        std::smatch matches;
        if (std::regex_search(line, matches, gameRegex))
        {
            team1 = matches[1];
            team2 = matches[2];
            score1 = std::stoi(matches[3]);
            score2 = std::stoi(matches[4]);
            std::cout << "Team 1: " << team1 << std::endl;
            std::cout << "Team 2: " << team2 << std::endl;
            std::cout << "Score 1: " << score1 << std::endl;
            std::cout << "Score 2: " << score2 << std::endl;

            //Add team1 to teams list if it does not exist
            auto team1It = std::find_if(teams.begin(), teams.end(), [&](const Team& t) { return t.getName() == team1; });
            if (team1It == teams.end()) 
            {
                teams.emplace_back(team1, 0, 0, 0);
                team1It = std::prev(teams.end());
            }

            auto team2It = std::find_if(teams.begin(), teams.end(), [&](const Team& t) { return t.getName() == team2; });
            if (team2It == teams.end())
            {
                teams.emplace_back(team2, 0, 0, 0);
                team2It = std::prev(teams.end());  
            }

            games.emplace_back(*team1It, *team2It, score1, score2);
        } else
        {
            std::cerr << "Invalid string!" << std::endl;
            // return 1;
        }
    }

    for(Team& team : teams)
        std::cout << "Team: " << team.getName() << std::endl;

    for(Game& game : games)
        std::cout << "Game: " << game.getTeam1().getName() << " - " << game.getTeam2().getName() << " " << game.getTeam1Score() << ":" << game.getTeam2Score() << std::endl;


    std::ofstream out("output.txt");

    for(const Team& team : teams)
    {
        out << team.getName() << std::endl;
        
        std::vector<Team*> opponents;
        for( Game* game : team.getGames() )
        {
            if( std::find( opponents.begin(), opponents.end(), &game->getTeam1() ) ==opponents.end() &&
                std::find( opponents.begin(), opponents.end(), &game->getTeam2() ) ==opponents.end()
            )
            {
                if( game->getTeam1().getName() == team.getName() )
                    opponents.push_back( &game->getTeam2() );

                if( game->getTeam2().getName() == team.getName() )
                    opponents.push_back( &game->getTeam1() );
            }
        }

        for(Team* opponent : opponents)
        {
            out << opponent->getName() << ' '

            << std::count_if( team.getGames().begin(), team.getGames().end(), [&](const Game* game) {
                return game->getTeam1().getName() == opponent->getName() || game->getTeam2().getName() == opponent->getName();
            }) << ' '

            << std::count_if( team.getGames().begin(), team.getGames().end(), [&](const Game* game) {
                return game->getTeam1().getName() == opponent->getName();
            }) << ' '

            << std::count_if( team.getGames().begin(), team.getGames().end(), [&](const Game* game) {
                if( game->getTeam1().getName() == opponent->getName() )
                    return game->getTeam1Score() < game->getTeam2Score();
                if( game->getTeam2().getName() == opponent->getName() )
                    return game->getTeam2Score() < game->getTeam1Score();
                return false;
            }) << ' '

            << std::accumulate( team.getGames().begin(), team.getGames().end(), 0, [&](int currSum, const Game* game) {
                if( game->getTeam1().getName() == opponent->getName() || game->getTeam2().getName() == opponent->getName() )
                {
                    if( game->getTeam1().getName() == team.getName() )
                        return currSum + game->getTeam1Score();
                    else
                        return currSum + game->getTeam2Score();
                }
                return (uint)currSum;
            } )
            << std::endl;
        }
    }
}