#include <iostream>
#include <fstream>
#include <vector>

template<typename NUM_TYPE>
class Vec2
{
    private:
        NUM_TYPE x;
        NUM_TYPE y;
    
    public:
        Vec2( NUM_TYPE x, NUM_TYPE y )
        {
            this->x = x;
            this->y = y;
        }

        Vec2(){}

        NUM_TYPE getX(){ return this->x; };
        NUM_TYPE getY(){ return this->y; };

        void setX( NUM_TYPE value ){ this->x = value; }
        void setY( NUM_TYPE value ){ this->y = value; }

        Vec2<NUM_TYPE> & operator+=( Vec2<NUM_TYPE> & addedVector )
        {
            this->x += addedVector.x;
            this->y += addedVector.y;

            return *this;
        }

        Vec2<NUM_TYPE> operator+( Vec2<NUM_TYPE> & addedVector )
        {
            Vec2<NUM_TYPE> result;

            result.setX(this->x + addedVector.x);
            result.setY(this->y + addedVector.y);

            return result;
        }

        void clockwise90Turn()
        {
            NUM_TYPE oldX = this->getX();
            this->setX( this->getY() );
            this->setY( oldX );
        }

        void counterClockwise90Turn()
        {
            NUM_TYPE oldX = this->getX();
            this->setX( -this->getY() );
            this->setY( -oldX );
        }
};

class Ball
{
    private:
        Vec2<int> pos;
        Vec2<int> speed;

    public:
        Ball( int x_pos, int y_pos, int x_speed, int y_speed )
        {
            this->pos = Vec2<int>(x_pos, y_pos);
            this->speed = Vec2<int>(x_speed, y_speed);
        }

        Ball(){}

        Vec2<int> getPos(){ return this->pos; };
        Vec2<int> getSpeed(){ return this->speed; };

        Vec2<int> incPos()
        {
            return (this->pos += this->speed);
        }

        Vec2<int> getNextPos()
        {
            return this->pos + this->speed;
        }

        void setSpeed( Vec2<int> newSpeed )
        {
            this->speed = newSpeed;
        }

        void invertSpeed()
        {
            this->speed.setX( -this->speed.getX() );
            this->speed.setY( -this->speed.getY() );
        }

        void turnRight()
        {
            this->speed.clockwise90Turn();
        }

        void turnLeft()
        {
            this->speed.counterClockwise90Turn();
        }
};

char accessStringAsArray( std::string & string, int x, int y, int width )
{
    return string.at(y * width + x + y + 1);
}

void accessStringAsArray( std::string & string, int x, int y, int width, char newValue )
{
    string[y * width + x + y + 1] = newValue;
}

void printField( int line, int width, std::string & field, Ball & ball )
{
    for( int y = 0; y < line; ++y )
    {
        for(int x = 0; x < width; ++x)
        {
            if( x == ball.getPos().getX() && y == ball.getPos().getY() )
                std::cout << '@';
            else
                std::cout << accessStringAsArray(field, x, y, width);
        }
        std::cout << std::endl;
    }
}

int main()
{
    std::string fileData;
    std::ifstream file("input.txt");

    std::string tmp;

    int line = 0;
    int width = 0;
    int prizesCount = 0;
    for( ; getline( file, tmp); ++line)
    {
        fileData += "\n" + tmp;
        width = tmp.size();
        for( char character : tmp )
            if( character == '*' ) prizesCount++;
    }

    Ball ball = Ball( 1,1, 1,0 );

    // std::cout << "========================\n"
    // << "Width: " << width << "\n"
    // << "Height: " << line << "\n"
    // << "Prizes: " << prizesCount << "\n"
    // << "========================\n";

    accessStringAsArray( fileData, width-2, line-2, width, '%' );

    std::ofstream resultOutputFile("output.txt");

    bool levelDone = false;
    int prizesCollected = 0;

    for( ; !levelDone; )
    {
        if( ball.getSpeed().getX() == 0 && ball.getSpeed().getY() == 0 )
            break;
        
        Vec2<int> nextBallPos = ball.incPos();

        // printField( line, width, fileData, ball );

        char standignOn = accessStringAsArray( fileData, nextBallPos.getX(), nextBallPos.getY(), width );
        switch( standignOn )
        {
            case ' ':
                break;

            case '*':
                prizesCollected++;
                break;

            case '#':
                ball.invertSpeed();
                break;
            
            case '/':
                ball.turnLeft();
                break;
            
            case '\\':
                ball.turnRight();
                break;

            case '%':
                levelDone = true;
                break;

            default:
                // std::cout<<"ERROR: Unknown character: " << accessStringAsArray( fileData, nextBallPos.getX(), nextBallPos.getY(), width );
                break;
        }

        // std::cout << "=========================\n"
        // << "On: " << standignOn << std::endl
        // << "Speed: [" << ball.getSpeed().getX() << ", " << ball.getSpeed().getY() << "]\n"
        // << "=========================\n";

        if( ball.getPos().getX() == 1 && ball.getPos().getY() == 1 )
            break;
    }


    if( levelDone )
    {
        resultOutputFile << "yes" << std::endl << ((prizesCollected >= prizesCount) ? "yes" : "no");
    } else
    {
        resultOutputFile << "no";
    }
}