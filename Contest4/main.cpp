#include <iostream>
#include <string.h>
#include <stdint.h>

template<typename T>
T * arraccess( T * arr, uint64_t w, uint64_t h, int i, int j )
{
    if( i < 0 ) i = (int)w + i;
    if( j < 0 ) j = (int)h + j;

    return &arr[ (i % w) + (j % h) * w ];
}

template<typename T>
void printUniverse( T * universe, uint64_t width, uint64_t height )
{
    for (size_t i = 0; i < width; i++)
    {
        for (size_t j = 0; j < height; j++)
        {
            std::cout << (*arraccess( universe, width, height, i, j ) ? "x" : " ");
        }
        std::cout << std::endl;
    }
}

template<typename T>
int checkAliveCellsAround( T * universe, uint64_t w, uint64_t h, int i, int j )
{
    int aliveCellsCount = 0;
    
    bool * checkedcell = arraccess( universe, w, h, i-1, j-1 );
    if( checkedcell != nullptr && *checkedcell )
        aliveCellsCount++;

    checkedcell = arraccess( universe, w, h, i, j-1 );
    if( checkedcell != nullptr && *checkedcell )
        aliveCellsCount++;

    checkedcell = arraccess( universe, w, h, i+1, j-1 );
    if( checkedcell != nullptr && *checkedcell )
        aliveCellsCount++;

    checkedcell = arraccess( universe, w, h, i-1, j );
    if( checkedcell != nullptr && *checkedcell )
        aliveCellsCount++;

    checkedcell = arraccess( universe, w, h, i+1, j );
    if( checkedcell != nullptr && *checkedcell )
        aliveCellsCount++;

    checkedcell = arraccess( universe, w, h, i-1, j+1 );
    if( checkedcell != nullptr && *checkedcell )
        aliveCellsCount++;

    checkedcell = arraccess( universe, w, h, i, j+1 );
    if( checkedcell != nullptr && *checkedcell )
        aliveCellsCount++;

    checkedcell = arraccess( universe, w, h, i+1, j+1 );
    if( checkedcell != nullptr && *checkedcell )
        aliveCellsCount++;

    return aliveCellsCount;
}

template<typename T>
int countAliveCellsIneUniverse( T * universe, uint64_t width, uint64_t height )
{
    int aliveCellsCount = 0;
    for (size_t i = 0; i < width; i++)
    {
        for (size_t j = 0; j < height; j++)
        {
            bool * currCell = arraccess( universe, width, height, i, j );

            if( *currCell )
                aliveCellsCount++;
        }
    }

    return aliveCellsCount;
}

int main()
{
    uint64_t width;
    uint64_t height;

    std::cin >> width;
    std::cin >> height;

    bool * universe = (bool *)malloc( sizeof(bool) * width * height );
    bool * universeNextState = (bool *)malloc( sizeof(bool) * width * height );

    for (size_t i = 0; i < width; i++)
    {
        for (size_t j = 0; j < height; j++)
        {
            std::cin >> universe[i + j * width];
        }
    }

    int t = 0;
    for( ; t < 1000; )
    {
        for (size_t i = 0; i < width; i++)
        {
            for (size_t j = 0; j < height; j++)
            {
                bool * currCell = arraccess( universe, width, height, i, j );
                int aliveCellsAround = checkAliveCellsAround( universe, width, height, i, j );

                bool * currCellInFuture = arraccess( universeNextState, width, height, i, j );

                *currCellInFuture = ( aliveCellsAround == 3 || ( *currCell && ( aliveCellsAround == 2 ) ) );
            }
        }

        t++;

        if( t == 1 || t == 10 || t == 100 || t == 1000 )
        {
            std::cout << "Step " << t << ": " << "alives " << countAliveCellsIneUniverse(universeNextState, width, height) <<std::endl;
            printUniverse( universeNextState, width, height );
        }

        if( countAliveCellsIneUniverse(universeNextState, width, height) <= 0 ||
            memcmp( universe, universeNextState, width*height ) == 0 )
        {
            break;
        }

        memcpy( universe, universeNextState, width*height );
    }

    std::cout << "End life step " << t;
}