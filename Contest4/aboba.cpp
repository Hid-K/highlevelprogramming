#include <iostream>
#include<string.h>
using namespace std;

int* arrayes(int *array, int width, int height,int x ,int y) {
    if (x < 0){
        x = width + x;
    }
    if (y < 0){
        y = height + y;
    }
    return &array[x%width + (y%height)*width];
}

void vivod(int *array, int width, int height)
{
    for (int i = 0; i < width; i++){
        for (int j = 0; j < height; j++){
            if (array[i + j*width])
                cout << "x";
            else
                cout << " ";
        }
        cout << endl;
    }
}

int proverka(int* universe,int N,int M, int i, int j){
    int* proverka = arrayes(universe, N, M, i - 1, j -1 );
    int schetchik_zhiz = 0;

    if(*proverka )
        schetchik_zhiz++;

    proverka = arrayes( universe, N, M, i, j-1 );
    if(*proverka )
        schetchik_zhiz++;

    proverka = arrayes( universe, N, M, i+1, j-1 );
    if(*proverka )
        schetchik_zhiz++;

    proverka = arrayes( universe, N, M, i-1, j );
    if(*proverka )
        schetchik_zhiz++;

    proverka = arrayes( universe, N, M, i+1, j );
    if(*proverka )
        schetchik_zhiz++;

    proverka = arrayes( universe, N, M, i-1, j+1 );
    if(*proverka )
        schetchik_zhiz++;

    proverka = arrayes( universe, N, M, i, j+1 );
    if(*proverka )
        schetchik_zhiz++;

    proverka = arrayes( universe, N, M, i+1, j+1 );
    if(*proverka )
        schetchik_zhiz++;

    return schetchik_zhiz;
}

void kolvososed (int* universe, int N, int M){
    for (int i = 0; i < N; i++){
        for (int j = 0; j < N; j++){
            cout << proverka(universe, N , M , i , j) << " ";
        }
        cout << endl;
    }
}
int pokol(int* universe, int  N,int M){
    int sum = 0;
    for (int i = 0; i < N*M;i++){
        sum += universe[i];
    }
    return sum;
}
bool srav(int * universe, int* new_universe,int N,int M){
    for (int i = 0; i < N; i++){
        for (int j = 0; j < M; j ++){
            if (universe[i + j*M] != new_universe[i+j*M]){
                return 0;
            }
        }
    }
    return 1;
}
int main() 
{
    int N, M;
    cin >> N >> M;
    int* universe = (int*)malloc(sizeof(int) * N * M);
    int* new_universe = (int*)malloc(sizeof(int) * N * M);
    for (int i = 0; i < N; i++)
    {
        for(int j = 0; j < M; ++j)
            cin >> universe[i + j * N];
    }
    // vivod(universe, N, M);
    int k = 0;
    for (; k < 1000;)
    {
        for (int i = 0; i < N; ++i)
        {
            for(int j = 0; j < M; ++j)
            {
                int* old_ele = arrayes(universe, N,M, i, j);
                int* ele = arrayes(new_universe, N,M, i, j);
                int sosed = proverka(universe ,N ,M ,i ,j);
                *ele  = ((sosed == 3) || ((sosed == 2) && (*old_ele == 1)));
            }
        }
        k++;
        int vizhivshie = pokol(new_universe, N , M);
        if (k == 1 || k == 10 || k == 100 || k == 1000)
        {
            cout << "Step " << k << ": "<< "alives " << vizhivshie << endl;
            vivod(new_universe, N, M);
        }
        if (vizhivshie == 0 || memcmp(universe, new_universe, N * M * sizeof(int)) == 0)
        {
            cout << vizhivshie << " " << srav(universe, new_universe, N, M);
            break;
        }
        for (int i = 0; i < N*M; i++)
        {
            universe[i] = new_universe[i];
        }
    }   
cout << "End life step " << k;
}