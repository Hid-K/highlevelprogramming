#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <algorithm> 

std::string reverseWords(const std::string& sentence)
{
    std::string result;
    std::vector<std::string> words;
    std::vector<int> spaces(100000);
    std::stringstream ss( sentence );

    std::string word;
    for ( ; ss >> word;  )
        words.push_back(word);

    int spacePointer = 0;
    for( auto&& word : words )
    {
        for( auto&& character : word )
            if( character == ' ' )
            {
                spaces[spacePointer]++;
            } else
                spacePointer++;
    }

    for (int i = 0; i < words.size(); i++)
    {
        if (i % 2 != 0)
        {
            result += words[i];
        } else
        {
            std::string reversedWord = words[i];
            std::reverse(reversedWord.begin(), reversedWord.end());
            result += reversedWord;
        }
        
        for( int i = 0; i < spaces[i]; ++i )
            result += " ";
    }

    return result;
}

int main() {
    std::string sentence;
    std::getline(std::cin, sentence);

    std::cout << reverseWords(sentence);

    return 0;
}