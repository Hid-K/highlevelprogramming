#include <iostream>
#include <string>

int main()
{
    std::string input = "";

    std::getline(std::cin, input);

    uint wordsCount = 0;

    for( int i = 0; i < 100000; ++i )
    {
        for( ; i < 100000 && input[i] == ' '; ++i );

        if(input[i] == '\0') break;

        wordsCount++;
        for( ; i < 100000 && input[i] != ' ' && input[i] != '\0'; ++i );
    }

    std::cout << wordsCount;
}