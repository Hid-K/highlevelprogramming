#include<iostream>
#include<string>

int main()
{
    std::string input = "";

    std::getline(std::cin, input);

    std::string result = "";

    char prevChar = input[0];
    result += input[0];
    for( char character : input )
    {
        if( character != prevChar )
            result += character;
        prevChar = character;
    }

    std::cout << result;
}