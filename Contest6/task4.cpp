#include<iostream>
#include<string>

int main()
{
    std::string input = "";

    std::getline(std::cin, input);

    input += '\0';

    std::string result = "";

    char prevChar = input[0];
    uint occurencies = 0;
    for( char& character : input )
    {
        if( character != prevChar || character == '\0' )
        {
            result += prevChar;
            if( occurencies > 1 )
            {
                result += "(";
                result += std::to_string( occurencies );
                result += ")";
            }
            occurencies = 0;
            prevChar = character;
        }
        occurencies++;
    }

    std::cout << result;
}