/**
 * Я не успел)
 */

#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <algorithm> 
#include <map>

int main()
{
    std::string input;
    std::getline(std::cin, input);
    
    std::stringstream ss( input );

    std::vector<std::string> words;

    std::string word;
    for( ; ss >>  word; )
        words.push_back(word);

    std::map<char, std::map<std::string, uint>> characterOccurency;

    for( std::string& word : words )
    {
        for( char character : word )
        {
            characterOccurency[character][word]++;
        }
    }

    for(
        std::map<char, std::map<std::string, uint>>::iterator iter = characterOccurency.begin();
        iter != characterOccurency.end();
        ++iter
    )
    {

    }
}